This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# React Medium Clone

This React medium board application is intended to publish articles. Equipped with registration, liking, tagging, front-side pagination options.

## Uses

- React
- React Router
- Context
- Hooks
- Axios
- Classnames

## Installation and usage

- Be sure that Git and Node.js are installed globally.
- Clone this repo.
- Run `npm install`, all required components will be installed automatically.
- Run `npm start` to start the project.
- Run `npm build` to create a build directory with a production build of the app.
- Run `npm test` to test the app.

## License

Under the terms of the MIT license.
